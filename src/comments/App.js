import React from "react"
import faker from "faker"

import CommentDetail from "./CommentDetail"
import ApprovalCard from "./ApprovalCard"

const App = () => {
  return (
    <div className="ui container comments">
      <ApprovalCard>
        <CommentDetail
          author="Sam"
          image={faker.image.avatar()}
          timeAgo="today 1:00AM"
          content="Nice blog post!"
        />
      </ApprovalCard>

      <ApprovalCard>
        <CommentDetail
          author="Juno"
          image={faker.image.avatar()}
          timeAgo="today 7:00AM"
          content="Hi there friends"
        />
      </ApprovalCard>

      <ApprovalCard>
        <CommentDetail
          author="Amara"
          image={faker.image.avatar()}
          timeAgo="today 7:00AM"
          content="This should be the last"
        />
      </ApprovalCard>
    </div>
  )
}

export default App
