import React from "react"
import ReactDOM from "react-dom"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import Comments from "./comments/App"
import Seasons from "./seassons/App"
import Pics from "./pics/App"

const App = () => {
  return (
    <Router>
      <Switch>
        <Route path="/comments">
          <Comments />
        </Route>

        <Route path="/classes">
          <Seasons />
        </Route>

        <Route path="/pics">
          <Pics />
        </Route>

        <Route path="/">
          <Seasons />
        </Route>
      </Switch>
    </Router>
  )
}

ReactDOM.render(<App />, document.querySelector("#root"))
