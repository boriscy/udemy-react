import axios from "axios"

export default axios.create({
  baseURL: "https://api.unsplash.com",
  headers: {
    Authorization: "Client-ID IWBeW5o98aT48hKBclfEHKtNuCuH-S9wKTwOZWI-AmY",
  },
})
