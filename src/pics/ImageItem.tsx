import React, { Component } from "react"

export interface ImageItemProps {
  id: string
  alt_description: string
  description: string
  urls: ImageUrlsInterface
  likes: number
  promoted_at: string
  width: number
  height: number
}

interface ImageUrlsInterface {
  thumb: string
  regular: string
  small: string
  raw: string
}
interface constProps {
  image: ImageItemProps
}

class ImageItem extends Component<
  { image: ImageItemProps },
  { spans: number; bottom: number }
> {
  imageRef: React.RefObject<HTMLImageElement> = React.createRef()
  state = { spans: 0, bottom: 0 }

  componentDidMount() {
    this.imageRef.current?.addEventListener("load", this.setSpans)
    window.onresize = () => {
      this.setSpans()
    }
  }

  setSpans = () => {
    const height: number = this.imageRef.current?.clientHeight || 0
    const spans = Math.ceil(height / 10)
    const bottom = 10 - (height % 10)
    this.setState({ spans, bottom })
  }

  render() {
    const { image } = this.props
    const { spans, bottom } = this.state
    return (
      <div className="item" style={{ gridRowEnd: `span ${spans}` }}>
        <a
          href={image.urls.raw}
          target="_blank"
          className="image"
          rel="noopener noreferrer"
        >
          <img
            src={image.urls.small}
            alt={image.alt_description}
            ref={this.imageRef}
          />
          <div className="content" style={{ bottom: `${bottom}px` }}>
            <h3 className="header">{image.alt_description}</h3>
            <div className="description">{image.description}</div>
            <div className="meta">
              <span>Width:</span> {image.width}
              &nbsp;
              <span>Height:</span> {image.height}
            </div>
          </div>
        </a>
      </div>
    )
  }
}

export default ImageItem
