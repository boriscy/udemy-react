import React, { Component } from "react"
import unsplashAPI from "../api/unsplash"
import SearchBar from "./SearchBar"
import ImageList from "./ImageList"
import { ImageItemProps } from "./ImageItem"
import "./pics.scss"

interface SearchInterface {
  term: string
}
interface ImagesInterface {
  images: ImageItemProps[]
  prevTerm: string
  page: number
}

class App extends Component<{}, ImagesInterface> {
  state: Readonly<ImagesInterface> = { images: [], page: 1, prevTerm: "" }

  async onSearchSubmit(search: SearchInterface) {
    const { term } = search
    const page = term === this.state.prevTerm ? this.state.page : 1
    const images = page === 1 ? [] : this.state.images

    console.log(page, term)
    const response = await unsplashAPI.get("/search/photos", {
      params: { query: term, page: page, per_page: 20 },
    })

    this.setState({
      images: images.concat(response.data.results),
      prevTerm: term,
      page: page + 1,
    })
  }

  render() {
    const { images } = this.state
    return (
      <div className="ui container pics-app" style={{ marginTop: "10px" }}>
        <SearchBar
          onSearchSubmit={(d: SearchInterface) => this.onSearchSubmit(d)}
        />
        <ImageList images={images} />
        {(images.length && (
          <button
            onClick={() => {
              this.onSearchSubmit({ term: this.state.prevTerm })
            }}
          >
            Get More
          </button>
        )) ||
          ""}
        <div className="total">Found {images.length}</div>
      </div>
    )
  }
}

export default App
