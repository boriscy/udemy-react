import React from "react"
import ImageItem, { ImageItemProps } from "./ImageItem"

interface ImageListProps {
  images: ImageItemProps[]
}

const ImageList: React.SFC<ImageListProps> = ({ images }) => {
  return (
    <div className="pics">
      <div className="image-list">
        {images.map((image) => (
          <ImageItem image={image} key={image.id} />
        ))}
      </div>
    </div>
  )
}

export default ImageList
