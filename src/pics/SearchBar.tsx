import React, { Component } from "react"

interface SearchBarProps {
  onSearchSubmit: Function
}
interface SearchBarState {
  term: string
}

class SearchBar extends Component<SearchBarProps, SearchBarState> {
  state: Readonly<SearchBarState> = { term: "" }

  onFormSubmit(e: React.FormEvent) {
    e.preventDefault()
    this.props.onSearchSubmit(this.state)
  }

  render() {
    const { term } = this.state
    return (
      <div className="ui segment">
        <form className="ui form" onSubmit={(e) => this.onFormSubmit(e)}>
          <div className="ui field">
            <label>Image Search</label>
            <input
              type="text"
              value={term}
              onChange={(e) => this.setState({ term: e.target.value })}
            />
          </div>
        </form>
      </div>
    )
  }
}

export default SearchBar
