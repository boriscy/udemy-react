import React from "react"
import "./style.css"

const seasonConfig = {
  summer: { text: "Lets hit the Beach", iconName: "sun" },
  winter: { text: "Burr, it's chilly", iconName: "snowflake" },
}

const getSeason = (latitude, month) => {
  if (month > 2 && month < 9) {
    return latitude > 0 ? "summer" : "winter"
  } else {
    return latitude > 0 ? "winter" : "summer"
  }
}

const SeasonDisplay = ({ latitude }) => {
  const month = new Date().getMonth()
  const season = getSeason(latitude, month)
  const { text, iconName } = seasonConfig[season]

  return (
    <div className={`season-${season}`}>
      <i className={`icon massive top ${iconName}`} />
      <div className="season-display">
        <h1>{text}</h1>
      </div>
      <i className={`icon massive bottom ${iconName}`} />
    </div>
  )
}

export default SeasonDisplay
