import React from "react"
import SeasonDisplay from "./SeasonDisplay"
import Loader from "./Loader"

class App extends React.Component {
  state = { latitude: null, longitude: null, errorMessage: "" }

  componentDidMount() {
    window.navigator.geolocation.getCurrentPosition(
      (pos) => {
        const { latitude, longitude } = pos.coords
        this.setState({ latitude, longitude })
      },
      (err) => {
        this.setState({ errorMessage: err.message })
      }
    )
  }

  renderContent() {
    const { latitude, errorMessage } = this.state

    if (this.state.errorMessage) {
      return (
        <div className="ui warning message">
          <div className="header">
            In order to make the app work please activate the geolocation
          </div>
          <p>{errorMessage}</p>
        </div>
      )
    }

    if (latitude) {
      return (
        <div>
          {/*
          <div>Latitude: {latitude}</div>
          <div>longitude: {longitude}</div>
          */}
          <SeasonDisplay latitude={latitude} />
        </div>
      )
    }

    return <Loader message="Please allow the to use Geolocation" />
  }

  render() {
    return this.renderContent()
  }
}

export default App
